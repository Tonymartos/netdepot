<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\Alice\Instances\Collection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="almacen")
 */

class Almacen
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombre;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $direccion;

    /**
     * @ORM\ManyToMany(targetEntity="Departamento")
     * @var Collection | Departamento[]
     */
    private $departamentos;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="almacenAlta")
     * @var Usuario
     */
    private $usuarioAlta;

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return Departamento[]|Collection
     */
    public function getDepartamentos()
    {
        return $this->departamentos;
    }

    /**
     * @param Departamento[]|Collection $departamentos
     */
    public function setDepartamentos($departamentos)
    {
        $this->departamentos = $departamentos;
    }

    /**
     * @return Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * @param Usuario $usuarioAlta
     */
    public function setUsuarioAlta($usuarioAlta)
    {
        $this->usuarioAlta = $usuarioAlta;
    }
}