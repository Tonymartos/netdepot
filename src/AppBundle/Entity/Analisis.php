<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="analisis")
 */
class Analisis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $estado;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $origen;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="analisis")
     * @var Producto
     */
    private $analisisProducto;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario",inversedBy="analisisCreados")
     * @var Usuario
     */
    private $analisisUsuario;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEstado()
    {
        return $this->estado;
    }

    /**
     * @param bool $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * @param string $origen
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;
    }

    /**
     * @return Producto
     */
    public function getAnalisisProducto()
    {
        return $this->analisisProducto;
    }

    /**
     * @param Producto $analisisProducto
     */
    public function setAnalisisProducto($analisisProducto)
    {
        $this->analisisProducto = $analisisProducto;
    }

    /**
     * @return Usuario
     */
    public function getAnalisisUsuario()
    {
        return $this->analisisUsuario;
    }

    /**
     * @param Usuario $analisisUsuario
     */
    public function setAnalisisUsuario($analisisUsuario)
    {
        $this->analisisUsuario = $analisisUsuario;
    }
}