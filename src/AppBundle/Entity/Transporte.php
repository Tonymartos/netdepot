<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\Alice\Instances\Collection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="transporte")
 */

class Transporte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombreCompleto;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $telefono;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $salidaProducto;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario",inversedBy="transporte")
     * @var Usuario
     */

    private $altaTransporte;

    /**
     * @ORM\OneToMany(targetEntity="Producto",mappedBy="transportista")
     * @var Collection | Producto[]
     */
    private $productoTransportado;


    public function __construct()
    {
        $this->productoTransportado = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNombreCompleto();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    /**
     * @param string $nombreCompleto
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isSalidaProducto()
    {
        return $this->salidaProducto;
    }

    /**
     * @param bool $salidaProducto
     */
    public function setSalidaProducto($salidaProducto)
    {
        $this->salidaProducto = $salidaProducto;
    }



    /**
     * @return Usuario
     */
    public function getAltaTransporte()
    {
        return $this->altaTransporte;
    }

    /**
     * @param Usuario $altaTransporte
     */
    public function setAltaTransporte($altaTransporte)
    {
        $this->altaTransporte = $altaTransporte;
    }

    /**
     * @return Producto[]|Collection
     */
    public function getProductoTransportado()
    {
        return $this->productoTransportado;
    }

    /**
     * @param Producto[]|Collection $productoTransportado
     */
    public function setProductoTransportado($productoTransportado)
    {
        $this->productoTransportado = $productoTransportado;
    }

}