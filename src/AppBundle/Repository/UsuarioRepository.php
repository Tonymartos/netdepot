<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends EntityRepository
{

    private function findByQueryBuilder()
    {
        return $this->createQueryBuilder('u')
            ->addSelect('p')
            ->leftJoin('u.productosAlta', 'p')
            ->orderBy('u.nombre');
    }

    public function findByNullProducto()
    {
        return $this->findByQueryBuilder()
            ->andWhere('SIZE(u.productosAlta)=0')
            ->getQuery()
            ->getResult();
    }

    public function findByAddProducto()
    {
        return $this->findByQueryBuilder()
        ->andWhere('SIZE(u.productosAlta)>0')
        ->getQuery()
        ->getResult();
    }
}