<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Producto;
use AppBundle\Form\ProductoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductoController extends Controller
{

    /**
     * @Route("/productos",name="productos_listar")
     */

    public function listarAction()
    {
        $productos = $this->getDoctrine()->getRepository('AppBundle:Producto')->findAll();

        return $this->render('productos/productos.html.twig', [
            'productos' => $productos
        ]);
    }
    /**
     * @Route("/productosTrasnporte/{id}",name="productos_transporte")
     */

    public function mostrarProductosTransporteAction(Producto $producto)
    {
        $transporte = $producto->getTransportista();
        return $this->render('productos/mostrar.html.twig',[
                'producto' => $producto,
                'transporte' => $transporte
            ]
        );
    }

    /**
     * @Route("/productos/nuevoproducto", name="productos_nuevo")
     */
    public function nuevoProductoAction(Request $request, Producto $producto=null)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProductoType::class,$producto);
        $form->handleRequest($request);

        if(null === $producto){
            $producto = new Producto();
            $producto->setNombreProducto("");
            $producto->setCantidad(0);
            $producto->setFabricante("");
            $em->persist($producto);
        }

        $form = $this->createForm(ProductoType::class,$producto);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('productos_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }

        return $this->render('productos/productoform.html.twig', [
            'producto' => $producto,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/productos/editar/{id}",name="productos_editar")
     */
    public function editarproductoAction(Request $request, Producto $producto)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProductoType::class,$producto);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('productos_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }
        return $this->render('productos/productoform.html.twig', [
            'producto' => $producto,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/productos/eliminar/{id}", name="productos_eliminar")
     */
    public function eliminarProductoAction(Request $request, Producto $producto)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            try {
                $em->remove($producto);
                $em->flush();
                return $this->redirectToRoute('productos_listar');
            }
            catch (\Exception $e) {
                $this->addFlash('error', 'No se ha podido eliminar el producto');
            }
        }

        return $this->render('productos/eliminarproducto.html.twig', [
            'producto' => $producto
        ]);
    }
}