<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180511115951 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE analisis (id INT AUTO_INCREMENT NOT NULL, analisis_producto_id INT DEFAULT NULL, analisis_usuario_id INT DEFAULT NULL, estado TINYINT(1) NOT NULL, origen VARCHAR(255) NOT NULL, INDEX IDX_502A90AFD1A0D322 (analisis_producto_id), INDEX IDX_502A90AF59A48E7A (analisis_usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE almacen_departamento (almacen_id INT NOT NULL, departamento_id INT NOT NULL, INDEX IDX_1D6DEA0F9C9C9E68 (almacen_id), INDEX IDX_1D6DEA0F5A91C08D (departamento_id), PRIMARY KEY(almacen_id, departamento_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departamento_almacen (departamento_id INT NOT NULL, almacen_id INT NOT NULL, INDEX IDX_800943755A91C08D (departamento_id), INDEX IDX_800943759C9C9E68 (almacen_id), PRIMARY KEY(departamento_id, almacen_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE analisis ADD CONSTRAINT FK_502A90AFD1A0D322 FOREIGN KEY (analisis_producto_id) REFERENCES producto (id)');
        $this->addSql('ALTER TABLE analisis ADD CONSTRAINT FK_502A90AF59A48E7A FOREIGN KEY (analisis_usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE almacen_departamento ADD CONSTRAINT FK_1D6DEA0F9C9C9E68 FOREIGN KEY (almacen_id) REFERENCES almacen (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE almacen_departamento ADD CONSTRAINT FK_1D6DEA0F5A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE departamento_almacen ADD CONSTRAINT FK_800943755A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE departamento_almacen ADD CONSTRAINT FK_800943759C9C9E68 FOREIGN KEY (almacen_id) REFERENCES almacen (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE producto ADD productos_departamento_id INT DEFAULT NULL, ADD usuario_alta_id INT DEFAULT NULL, ADD transportista_id INT DEFAULT NULL, DROP estado');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB0615F250DE05 FOREIGN KEY (productos_departamento_id) REFERENCES departamento (id)');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB0615A0753702 FOREIGN KEY (usuario_alta_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB061552F4F166 FOREIGN KEY (transportista_id) REFERENCES transporte (id)');
        $this->addSql('CREATE INDEX IDX_A7BB0615F250DE05 ON producto (productos_departamento_id)');
        $this->addSql('CREATE INDEX IDX_A7BB0615A0753702 ON producto (usuario_alta_id)');
        $this->addSql('CREATE INDEX IDX_A7BB061552F4F166 ON producto (transportista_id)');
        $this->addSql('ALTER TABLE comentarioProducto ADD usuario_id INT DEFAULT NULL, ADD producto_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comentarioProducto ADD CONSTRAINT FK_23F65719DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE comentarioProducto ADD CONSTRAINT FK_23F657197645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('CREATE INDEX IDX_23F65719DB38439E ON comentarioProducto (usuario_id)');
        $this->addSql('CREATE INDEX IDX_23F657197645698E ON comentarioProducto (producto_id)');
        $this->addSql('ALTER TABLE almacen ADD almacen_alta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE almacen ADD CONSTRAINT FK_D5B2D250D276DFC5 FOREIGN KEY (almacen_alta_id) REFERENCES usuario (id)');
        $this->addSql('CREATE INDEX IDX_D5B2D250D276DFC5 ON almacen (almacen_alta_id)');
        $this->addSql('ALTER TABLE transporte ADD alta_transporte_id INT DEFAULT NULL, ADD salida_producto TINYINT(1) NOT NULL, CHANGE nombrecompleto nombre_completo VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE transporte ADD CONSTRAINT FK_336ADCB423FA4745 FOREIGN KEY (alta_transporte_id) REFERENCES usuario (id)');
        $this->addSql('CREATE INDEX IDX_336ADCB423FA4745 ON transporte (alta_transporte_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE analisis');
        $this->addSql('DROP TABLE almacen_departamento');
        $this->addSql('DROP TABLE departamento_almacen');
        $this->addSql('ALTER TABLE almacen DROP FOREIGN KEY FK_D5B2D250D276DFC5');
        $this->addSql('DROP INDEX IDX_D5B2D250D276DFC5 ON almacen');
        $this->addSql('ALTER TABLE almacen DROP almacen_alta_id');
        $this->addSql('ALTER TABLE comentarioProducto DROP FOREIGN KEY FK_23F65719DB38439E');
        $this->addSql('ALTER TABLE comentarioProducto DROP FOREIGN KEY FK_23F657197645698E');
        $this->addSql('DROP INDEX IDX_23F65719DB38439E ON comentarioProducto');
        $this->addSql('DROP INDEX IDX_23F657197645698E ON comentarioProducto');
        $this->addSql('ALTER TABLE comentarioProducto DROP usuario_id, DROP producto_id');
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB0615F250DE05');
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB0615A0753702');
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB061552F4F166');
        $this->addSql('DROP INDEX IDX_A7BB0615F250DE05 ON producto');
        $this->addSql('DROP INDEX IDX_A7BB0615A0753702 ON producto');
        $this->addSql('DROP INDEX IDX_A7BB061552F4F166 ON producto');
        $this->addSql('ALTER TABLE producto ADD estado TINYINT(1) NOT NULL, DROP productos_departamento_id, DROP usuario_alta_id, DROP transportista_id');
        $this->addSql('ALTER TABLE transporte DROP FOREIGN KEY FK_336ADCB423FA4745');
        $this->addSql('DROP INDEX IDX_336ADCB423FA4745 ON transporte');
        $this->addSql('ALTER TABLE transporte DROP alta_transporte_id, DROP salida_producto, CHANGE nombre_completo nombrecompleto VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
