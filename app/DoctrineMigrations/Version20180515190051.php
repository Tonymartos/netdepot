<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180515190051 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE almacen_departamento');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE almacen_departamento (almacen_id INT NOT NULL, departamento_id INT NOT NULL, INDEX IDX_1D6DEA0F9C9C9E68 (almacen_id), INDEX IDX_1D6DEA0F5A91C08D (departamento_id), PRIMARY KEY(almacen_id, departamento_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE almacen_departamento ADD CONSTRAINT FK_1D6DEA0F5A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE almacen_departamento ADD CONSTRAINT FK_1D6DEA0F9C9C9E68 FOREIGN KEY (almacen_id) REFERENCES almacen (id) ON DELETE CASCADE');
    }
}
