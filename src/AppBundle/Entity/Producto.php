<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\Alice\Instances\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="producto")
 */

class Producto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombreProducto;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $cantidad;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $fabricante;

    /**
     * @ORM\ManyToOne(targetEntity="Departamento", inversedBy="productosAlmacenados")
     * @var Departamento
     */
    private $productosDepartamento;

    /**
     * @ORM\OneToMany(targetEntity="Analisis", mappedBy="analisisProducto")
     * @var Analisis
     */
    private $analisis;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario",inversedBy="productosAlta")
     * @var Usuario
     */
    private $usuarioAlta;

    /**
     * @ORM\ManyToOne(targetEntity="Transporte", inversedBy="productoTransportado")
     * @var Transporte
     */
    private $transportista;

    /**
     * @ORM\OneToMany(targetEntity="Comentario", mappedBy="producto")
     * @var Collection | Comentario[]
     */
    private $comentarios;

    public function __construct()
    {
        $this->comentarios = new ArrayCollection();
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombreProducto()
    {
        return $this->nombreProducto;
    }

    /**
     * @param string $nombreProducto
     */
    public function setNombreProducto($nombreProducto)
    {
        $this->nombreProducto = $nombreProducto;
    }

    /**
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * @param string $fabricante
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;
    }

    /**
     * @return Departamento
     */
    public function getProductosDepartamento()
    {
        return $this->productosDepartamento;
    }

    /**
     * @param Departamento $productosDepartamento
     */
    public function setProductosDepartamento($productosDepartamento)
    {
        $this->productosDepartamento = $productosDepartamento;
    }

    /**
     * @return Analisis
     */
    public function getAnalisis()
    {
        return $this->analisis;
    }

    /**
     * @param Analisis $analisis
     */
    public function setAnalisis($analisis)
    {
        $this->analisis = $analisis;
    }

    /**
     * @return Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * @param Usuario $usuarioAlta
     */
    public function setUsuarioAlta($usuarioAlta)
    {
        $this->usuarioAlta = $usuarioAlta;
    }

    /**
     * @return Transporte
     */
    public function getTransportista()
    {
        return $this->transportista;
    }

    /**
     * @param Transporte $transportista
     */
    public function setTransportista($transportista)
    {
        $this->transportista = $transportista;
    }

    /**
     * @return Comentario[]|Collection
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param Comentario[]|Collection $comentarios
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    }

}