<?php


namespace AppBundle\Form;


use Symfony\Component\Form\FormBuilderInterface;

class CambioClaveType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', null,[
                'label' => 'Cambiar Contraseña:',
            ]);
    }
}