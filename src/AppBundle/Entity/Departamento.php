<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\Alice\Instances\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="departamento")
 */

class Departamento
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Assert\Length(
     *     min="4",
     *     max="12",
     *     min="Mínimo 4 caracteres para el nombre"
     * )
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombre;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="Almacen")
     * @var Collection | Almacen[]
     */
    private $almacenes;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="productosDepartamento")
     * @var Collection | Producto[]
     */
    private $productosAlmacenados;

    public function __construct()
    {
        $this->almacenes = new ArrayCollection();
        $this->productosAlmacenados = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return Almacen[]|Collection
     */
    public function getAlmacenes()
    {
        return $this->almacenes;
    }

    /**
     * @param Almacen[]|Collection $almacenes
     */
    public function setAlmacenes($almacenes)
    {
        $this->almacenes = $almacenes;
    }

    /**
     * @return Producto[]|Collection
     */
    public function getProductosAlmacenados()
    {
        return $this->productosAlmacenados;
    }

    /**
     * @param Producto[]|Collection $productosAlmacenados
     */
    public function setProductosAlmacenados($productosAlmacenados)
    {
        $this->productosAlmacenados = $productosAlmacenados;
    }
}