<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use AppBundle\Form\CambioClaveType;
use AppBundle\Form\UsuarioType;
use AppBundle\Security\UsuarioVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioController extends Controller
{
    /**
     * @Route("/usuarios/sinregistros", name="usuarios_sinregistros")
     */
    public function listarUsuariosNullProductos()
    {
        $usuarios = $this->getDoctrine()->getRepository('AppBundle:Usuario')->findByNullProducto();

        return $this->render('usuarios/sin_productos_registrados.html.twig', [
           'usuarios'=>$usuarios
        ]);
    }

    /**
     * @Route("/usuarios/conregistros", name="usuarios_conregistros")
     */
    public function listarUsuariosConProductos()
    {
        $usuarios = $this->getDoctrine()->getRepository('AppBundle:Usuario')->findByAddProducto();


        return $this->render('usuarios/con_productos_registrados.html.twig', [
            'usuarios'=>$usuarios
        ]);
    }

    /**
     * @Route("/usuarios", name="usuarios_listar")
     */
    public function listaUsuariosAll()
    {
        $usuarios = $this->getDoctrine()->getRepository('AppBundle:Usuario')->findAll();

        return $this->render('usuarios/usuarios.html.twig', [
            'usuarios' => $usuarios
        ]);
    }

    /**
     * @Route("/usuarios/nuevousuario", name="usuarios_nuevo")
     */
    public function nuevoUsuarioAction(Request $request, Usuario $usuario=null)
    {
        $em = $this->getDoctrine()->getManager();

        if(null === $usuario){
            $usuario = new Usuario();
            $usuario->setNombre("");
            $usuario->setNombreUsuario("");
            $usuario->setEmail("");
            $usuario->setPassword("");
            $usuario->setEmpleado(false);
            $usuario->setSupervisor(false);
            $usuario->setActivo(true);
            $usuario->setAdministrador(false);
            $em->persist($usuario);
        }

        $form = $this->createForm(UsuarioType::class,$usuario,[
            'admin' => $this->isGranted('ROLE_ADMIN')
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('usuarios_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }

        return $this->render('usuarios/form.html.twig', [
            'usuario' => $usuario,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/usuarios/editar/{id}",name="usuarios_editar")
     */
    public function editarUsuarioAction(Request $request, Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UsuarioType::class,$usuario,[
            'admin' => $this->isGranted('ROLE_ADMIN')
        ]);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('usuarios_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }
        return $this->render('usuarios/form.html.twig', [
            'usuario' => $usuario,
            'formulario' => $form->createView()
        ]);
    }


    /**
    * @Route("/usuario/eliminar/{id}", name="usuarios_eliminar")
    */
        public function eliminarUsuarioAction(Request $request, Usuario $usuario)
        {
            $em = $this->getDoctrine()->getManager();

            if ($request->isMethod('POST')) {
                try {
                    $em->remove($usuario);
                    $em->flush();
                    return $this->redirectToRoute('usuarios_listar');
                }
                catch (\Exception $e) {
                    $this->addFlash('error', 'No se ha podido eliminar el usuario');
                }
            }

            return $this->render('usuarios/eliminarusuario.html.twig', [
                'usuario' => $usuario
            ]);
        }

    /**
     * @Route("/usuario/clave", name="usuario_cambiar_clave")
     * @Security("is_granted('ROLE_USER')")
     */
        public function cambiarClave(Request $request, UserPasswordEncoderInterface $userPasswordEncoder)
        {
            /** @var Usuario $usuario */
            $usuario = $this->getUser();

            $formulario = $this->createForm(CambioClaveType::class, $usuario);

            $formulario->handleRequest($request);

            if($formulario->isSubmitted() && $formulario->isValid()){
                $textoPlano = $formulario->get('nuevaClase')->getData();

                try{
                    $usuario->setPassword($userPasswordEncoder->encodePassword($usuario, $textoPlano)
                    );
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('info', 'Contraseña cambiada con exito');

                    return $this->redirectToRoute('Inicio');
                }
                catch (\Exception $e){
                    $this->addFlash('error', 'No se han podido guardar los cambios');
                }
            }

            return $this->render('usuarios/cambio_clave.html.twig', [
                'formulario' => $formulario->createView()
            ]);
        }
}
