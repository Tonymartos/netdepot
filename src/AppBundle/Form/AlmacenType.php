<?php


namespace AppBundle\Form;


use AppBundle\Entity\Almacen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlmacenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,[
                'label' => 'Nombre de Almacén: '
            ])
            ->add('direccion', null,[
                'label' => 'Dirección: '
            ])
            ->add('departamentos',null,[
                'label' => 'Departamentos: ',
                'expanded' => true,
                'multiple' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Almacen::class,
        ]);
    }
}