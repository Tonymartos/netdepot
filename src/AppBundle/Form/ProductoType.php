<?php


namespace AppBundle\Form;

use AppBundle\Entity\Producto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreProducto', null,[
                'label' => 'Nombre de Producto: '
            ])
            ->add('cantidad', null,[
                'label' => 'Cantidad de lotes: '
            ])
            ->add('fabricante', null,[
                'label' => 'Fabricante:'
                ])
            ->add('transportista', null,[
                'label' => 'Transportista:'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producto::class,
        ]);
    }
}