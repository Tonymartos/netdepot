<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 6/06/18
 * Time: 12:29
 */

namespace AppBundle\Security;


use AppBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UsuarioVoter extends Voter
{

    const VER = 'USUARIO_VER';
    const MODIFICAR = 'USUARIO_MODIFICAR';
    const ELIMINAR = 'USUARIO_ELIMINAR';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!$subject instanceof Usuario)
        {
            return false;
        }

        if (!in_array($attribute, [self::VER, self::MODIFICAR], true)){
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if(!$user instanceof Usuario){
            return false;
        }

        if($this->decisionManager->decide($token, ['ROLE_ADMIN'])){
            return true;
        }

        switch ($attribute){
            case self::VER:
                return $this->puedeVer($subject, $token, $user);
            case self::MODIFICAR:
                return $this->puedeModificar($subject, $token, $user);
        }

        return false;
    }

    private function puedeVer(Usuario $producto, TokenInterface $token, Usuario $user)
    {

            return true;


    }


    private function puedeModificar(Usuario $c, TokenInterface $token, Usuario $user)
    {
        if($c->getId() === $user->getId()){
            return true;
        }

        return $this->decisionManager->decide($token, ['ROLE_SUPERVISOR']);
    }
}