<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180515132834 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE almacen DROP FOREIGN KEY FK_D5B2D250D276DFC5');
        $this->addSql('DROP INDEX IDX_D5B2D250D276DFC5 ON almacen');
        $this->addSql('ALTER TABLE almacen CHANGE almacen_alta_id usuario_alta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE almacen ADD CONSTRAINT FK_D5B2D250A0753702 FOREIGN KEY (usuario_alta_id) REFERENCES usuario (id)');
        $this->addSql('CREATE INDEX IDX_D5B2D250A0753702 ON almacen (usuario_alta_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE almacen DROP FOREIGN KEY FK_D5B2D250A0753702');
        $this->addSql('DROP INDEX IDX_D5B2D250A0753702 ON almacen');
        $this->addSql('ALTER TABLE almacen CHANGE usuario_alta_id almacen_alta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE almacen ADD CONSTRAINT FK_D5B2D250D276DFC5 FOREIGN KEY (almacen_alta_id) REFERENCES usuario (id)');
        $this->addSql('CREATE INDEX IDX_D5B2D250D276DFC5 ON almacen (almacen_alta_id)');
    }
}
