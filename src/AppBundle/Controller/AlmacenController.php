<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Almacen;
use AppBundle\Form\AlmacenType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AlmacenController extends Controller
{
    /**
     * @Route("/almacenes",name="almacenes_listar")
     */
    public function listarAction()
    {
        $almacenes = $this->getDoctrine()->getRepository('AppBundle:Almacen')->findAll();

        return $this->render('almacenes/almacenes.html.twig', [
            'almacenes' => $almacenes
        ]);
    }

    /**
     * @Route("/AlmacenDepartamento/{id}", name="almacen_departamento")
     */
    public function mostrarAlmacenDepartamentoAction(Almacen $almacen)
    {
        $departamento = $almacen->getDepartamentos();
        return $this->render('almacenes/almacenDepartamento.html.twig',[
            'departamento' => $departamento,
            'almacen' => $almacen
        ]);
    }

    /**
     * @Route("/almacen/nuevoalmacen", name="almacenes_nuevo")
     */
    public function nuevoAlmacenAction(Request $request, Almacen $almacen=null)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(AlmacenType::class,$almacen);
        $form->handleRequest($request);
        if(null === $almacen){
            $almacen = new Almacen();
            $almacen->setNombre("");
            $almacen->setDireccion("");
            $em->persist($almacen);
        }
        $form = $this->createForm(AlmacenType::class,$almacen);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('almacenes_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }
        return $this->render('almacenes/almacenesform.html.twig', [
            'almacen' => $almacen,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/almacen/{id}", name="almacenes_editar")
     */
    public function editarAlmacenAction(Request $request, Almacen $almacen)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(AlmacenType::class,$almacen);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('almacenes_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }
        return $this->render('almacenes/almacenesform.html.twig', [
            'almacen' => $almacen,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/almacen/eliminar/{id}", name="almacenes_eliminar")
     */
    public function eliminarAlmacenAction(Request $request, Almacen $almacen)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            try {
                $em->remove($almacen);
                $em->flush();
                return $this->redirectToRoute('almacenes_listar');
            }
            catch (\Exception $e) {
                $this->addFlash('error', 'No se ha podido eliminar el almacén');
            }
        }

        return $this->render('almacenes/eliminaralmacen.html.twig', [
            'almacen' => $almacen
        ]);
    }
}
