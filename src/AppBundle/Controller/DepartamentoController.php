<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Departamento;
use AppBundle\Form\DepartamentoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DepartamentoController extends Controller
{
    /**
     * @Route("/departamentos", name="departamentos_listar")
     */

    public function listarAction()
    {
        $departamentos = $this->getDoctrine()->getRepository('AppBundle:Departamento')->findAll();

        return $this->render('departamentos/departamentos.html.twig', [
            'departamentos' => $departamentos
        ]);
    }


    /**
     * @Route("/departamentos/nuevodepartamento", name="departamentos_nuevo")
     */
    public function nuevoDepartamentoAction(Request $request, Departamento $departamento=null)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(DepartamentoType::class,$departamento);
        $form->handleRequest($request);

        if(null === $departamento){
            $departamento = new Departamento();
            $departamento-> setNombre("");
            $departamento-> setDescripcion("");
            $em->persist($departamento);
        }

        $form = $this->createForm(DepartamentoType::class,$departamento);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('departamentos_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }

        return $this->render('departamentos/departamentoform.html.twig', [
            'departamento' => $departamento,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/departamentos/editar/{id}",name="departamentos_editar")
     */
    public function editarDepartamentoAction(Request $request, Departamento $departamento)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(DepartamentoType::class,$departamento);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try{
                $em->flush();
                return $this->redirectToRoute('departamentos_listar');
            } catch (\Exception $e) {
                $this->addFlash('error', 'No se han podido guardar los cambios');
            }
        }
        return $this->render('departamentos/departamentoform.html.twig', [
            'departamento' => $departamento,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/departamentoAlmacen/{id}", name="departamento_almacen")
     */
    public function mostrarDepartamentoAlmacenAction(Departamento $departamento)
    {
        $almacen = $departamento->getAlmacenes();
        return $this->render('departamentos/departamentoAlmacen.html.twig',[
            'departamento' => $departamento,
            'almacen' => $almacen
        ]);
    }

    /**
     * @Route("/departamento/eliminar/{id}", name="departamentos_eliminar")
     */
    public function eliminardepartamentoAction(Request $request, Departamento $departamento)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            try {
                $em->remove($departamento);
                $em->flush();
                return $this->redirectToRoute('departamentos_listar');
            }
            catch (\Exception $e) {
                $this->addFlash('error', 'No se ha podido eliminar el departamento');
            }
        }

        return $this->render('departamentos/eliminardepartamento.html.twig', [
            'departamento' => $departamento
        ]);
    }

}