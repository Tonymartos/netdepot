<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Transporte;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TransportistaController extends Controller
{
    /**
     * @Route("/transportes",name="transportes_listar")
     */
    public function listarAction()
    {
        $transportes = $this->getDoctrine()->getRepository('AppBundle:Transporte')->findAll();

        return $this->render('transportes/transportes.html.twig', [
            'transportes' => $transportes
        ]);
    }

    /**
     * @Route("/transportes/{id}", name="transportes_mostrar")
     */
    public function mostrarAction(Transporte $transporte)
    {
        return $this->render('transportes/mostrartransporte.html.twig',[
                'transporte' => $transporte
            ]
        );

    }
}
