<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 6/06/18
 * Time: 12:29
 */

namespace AppBundle\Security;


use AppBundle\Entity\Producto;
use AppBundle\Entity\Usuario;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class ProductoVoter
{

    const VER = 'PRODUCTO_VER';
    const MODIFICAR = 'PRODUCTO_MODIFICAR';
    const ELIMINAR = 'PRODUCTO_ELIMINAR';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!$subject instanceof Producto)
        {
            return false;
        }

        if (!in_array($attribute, [self::VER, self::MODIFICAR, self::ELIMINAR], true)){
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if(!$user instanceof Usuario){
            return false;
        }

        if($this->decisionManager->decide($token, ['ROLE_ADMIN'])){
            return true;
        }

        switch ($attribute){
            case self::VER:
                return $this->puedeVer($subject, $token, $user);
            case self::MODIFICAR:
                return $this->puedeModificar($subject, $token, $user);
            case self::ELIMINAR:
                return $this->puedeEliminar($subject,$token,$user);
        }

        return false;
    }

    private function puedeVer(Producto $producto, TokenInterface $token, Usuario $user)
    {
        if($producto->getUsuarioAlta() === $user){
            return true;
        }

    }

    private function puedeModificar(Producto $producto, TokenInterface $token, Usuario $user)
    {
        if($producto->getUsuarioAlta() === $user){
            return true;
        }
        return $this->decisionManager->decide($token, ['ROLE_SUPERVISOR']);
    }

    private function puedeEliminar(Producto $producto, TokenInterface $token, Usuario $user)
    {
        if($producto->getUsuarioAlta() === $user){
            return true;
        }
        return $this->decisionManager->decide($token, ['ROLE_SUPERVISOR']);
    }
}