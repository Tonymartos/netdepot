NetDepot
=========

**Aplicación Web** de gestión logistica para grandes almacenes.
 
 
** Compilación del proyecto

- Para este proyecto es necesario tener una base de datos MySql, una vez creado la base de datos procederemos a los siguientes pasos:

- Instalar las dependencias con Composer.
`composer install`

Accederá al archivo composer.json, leerá todas las dependencias con sus respectivas versiones y las descargará

Una vez que ha instalado todas las dependencias te pedirá el host, puerto, nombre de la base de datos y la contraseña.

- Si aparece algun tipo de error con AppBundle u otro en ese caso debemos actualizar con composer:

`composer update`

- Una vez conectada la base de datos, migraremos las clases entidades con la base de datos para crear las tablas:

Para la migración se procedera a introducir el siguiente comando

`php bin/console doctrine:migrations:migrate`

Para rellenar las tablas de la base de datos utilizaremos el comando:

`php bin/console doctrine:fixtures:load`


Para arrancar el servidor web solo tenemos que introducir el siguiente comando:

`php bin/console server:run`