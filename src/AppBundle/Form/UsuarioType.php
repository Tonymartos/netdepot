<?php

namespace AppBundle\Form;


use AppBundle\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreUsuario', null,[
                'label' => 'Nombre de usuario:'
            ])
            ->add('email', null,[
                'label' => 'Correo electrónico:'
            ])
            ->add('password', null,[
                'label' => 'Contraseña:',
                'disabled' => !$options['admin']
            ])
            ->add('nombre', null,[
                'label' => 'Nombre:'
            ])
            ->add('apellido', null,[
                'label' => 'Apellidos:'
            ])
            ->add('activo', null,[

            ])
            ->add('empleado', null,[
                'label' => 'Empleado'
            ])

            ->add('supervisor', null,[
            'label' => 'Supervisor'
            ])

            ->add('administrador', null,[
                'label' => 'Administrador'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
            'admin' => false
        ]);
    }
}