<?php


namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\Alice\Instances\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 * @ORM\Table(name="usuario")
 */

class Usuario implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @Assert\NotNull()
     * @Assert\Regex(
     *     pattern="/\W/",
     *     match=false,
     *     message="Solo se permite palabras minusculas y letras"
     *
     * )
     * @Assert\Length(
     *     min="4",
     *     max="12",
     *     minMessage="Minimo 4 caracteres para el nombre de usuario"
     * )
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombreUsuario;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $password;
    /**
     * @Assert\Email(message = "El correo electrónico '{{ value }}' no es válido.",checkMX = true)
     * @ORM\Column(type="string")
     * @var string
     */
    private $email;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $nombre;
    /**
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     * @var string
     */
    private $apellido;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $activo;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $administrador;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $supervisor;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $empleado;

    /**
     * @ORM\OneToMany(targetEntity="Producto",mappedBy="usuarioAlta")
     * @var Collection | Producto[]
     */
    private $productosAlta;

    /**
     * @ORM\OneToMany(targetEntity="Transporte",mappedBy="altaTransporte")
     * @var Collection | Transporte[]
     */
    private $transporte;

    /**
     * @ORM\OneToMany(targetEntity="Analisis", mappedBy="analisisUsuario")
     * @var Collection | Analisis[]
     */
    private $analisisCreados;

    /**
     * @ORM\OneToMany(targetEntity="Comentario", mappedBy="usuario")
     * @var Collection | Comentario[]
     */
    private $comentariosUsuario;

    /**
     * @ORM\OneToMany(targetEntity="Almacen", mappedBy="usuarioAlta")
     * @var Collection | Almacen[]
     */
    private $almacenAlta;

    public function __construct()
    {
        $this->productosAlta = new ArrayCollection();
        $this->transporte = new ArrayCollection();
        $this->analisisCreados = new ArrayCollection();
        $this->comentariosUsuario = new ArrayCollection();
        $this->almacenAlta = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombreUsuario()
    {
        return $this->nombreUsuario;
    }

    /**
     * @param string $nombreUsuario
     */
    public function setNombreUsuario($nombreUsuario)
    {
        $this->nombreUsuario = $nombreUsuario;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return bool
     */
    public function isActivo()
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return bool
     */
    public function isAdministrador()
    {
        return $this->administrador;
    }

    /**
     * @param bool $administrador
     */
    public function setAdministrador($administrador)
    {
        $this->administrador = $administrador;
    }

    /**
     * @return bool
     */
    public function isSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * @param bool $supervisor
     */
    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;
    }

    /**
     * @return bool
     */
    public function isEmpleado()
    {
        return $this->empleado;
    }

    /**
     * @param bool $empleado
     */
    public function setEmpleado($empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return Producto[]|Collection
     */
    public function getProductosAlta()
    {
        return $this->productosAlta;
    }

    /**
     * @param Producto[]|Collection $productosAlta
     */
    public function setProductosAlta($productosAlta)
    {
        $this->productosAlta = $productosAlta;
    }

    /**
     * @return Transporte[]|Collection
     */
    public function getTransporte()
    {
        return $this->transporte;
    }

    /**
     * @param Transporte[]|Collection $transporte
     */
    public function setTransporte($transporte)
    {
        $this->transporte = $transporte;
    }

    /**
     * @return Analisis[]|Collection
     */
    public function getAnalisisCreados()
    {
        return $this->analisisCreados;
    }

    /**
     * @param Analisis[]|Collection $analisisCreados
     */
    public function setAnalisisCreados($analisisCreados)
    {
        $this->analisisCreados = $analisisCreados;
    }

    /**
     * @return Comentario[]|Collection
     */
    public function getComentariosUsuario()
    {
        return $this->comentariosUsuario;
    }

    /**
     * @param Comentario[]|Collection $comentariosUsuario
     */
    public function setComentariosUsuario($comentariosUsuario)
    {
        $this->comentariosUsuario = $comentariosUsuario;
    }

    /**
     * @return Almacen[]|Collection
     */
    public function getAlmacenAlta()
    {
        return $this->almacenAlta;
    }

    /**
     * @param Almacen[]|Collection $almacenAlta
     */
    public function setAlmacenAlta($almacenAlta)
    {
        $this->almacenAlta = $almacenAlta;
    }

    public function getRoles()
    {
        $roles = ['ROLE_USER'];

        if ($this->isAdministrador()){
            $roles[] = 'ROLE_ADMIN';
        }

        if ($this->isSupervisor()){
            $roles[] = 'ROLE_SUPERVISOR';
        }

        if ($this->isEmpleado()){
            $roles[] = 'ROLE_EMPLEADO';
        }

        return $roles;
    }

    public function getUsername()
    {
        return $this->getNombreUsuario();
    }

    public function getSalt()
    {
        return $this->getNombreUsuario();
    }

    public function eraseCredentials()
    {
    }
}